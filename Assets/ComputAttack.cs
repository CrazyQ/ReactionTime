﻿using UnityEngine;
using System.Collections;

public enum HitState
{
    oneHit,
    doubleHit,
    miss,
    threeHit
}
public class ComputAttack : MonoBehaviour {
    public GameObject lbTip;

    public Collider2D startPoint,endPoint,oneHitArea,doubleHitArea;
    public Rigidbody2D _rig2;
    public float speed = 1f;
    HitState nowHit = HitState.miss;
    
	void Start () {
        ResetVelocity();
	}
	void ResetVelocity()
    {
        nowHit = HitState.miss;
        this.transform.position = startPoint.transform.position;
        _rig2.velocity = new Vector2(speed, 0); 

    }
	void Update () {
        if(Input.GetKeyDown(KeyCode.J))
        {
            _rig2.velocity = Vector2.zero;
            SList.l.showDamage.ShowHitPoint(nowHit.ToString());
            Invoke("ResetVelocity", 2f);
        }
	}

    void OnTriggerEnter2D(Collider2D obj)
    {
        if(obj.Equals(endPoint))
        {
            this.transform.position = startPoint.transform.position;
        }
        else if(obj.Equals(oneHitArea))
        {
            nowHit = HitState.oneHit;
            Debug.Log("good");
        }
        else if (obj.Equals(doubleHitArea))
        {
            nowHit = HitState.doubleHit;
        }
    }

    void OnTriggerExit2D(Collider2D obj)
    {
        if (obj.Equals(doubleHitArea))
        {
            nowHit = HitState.oneHit;
        }
        else if (obj.Equals(oneHitArea))
        {
            nowHit = HitState.miss;
        }
    }


}
