﻿using UnityEngine;
using System.Collections;

public class ShowDamage : MonoBehaviour {

    TweenColor ta;
    UILabel lbTxt;
	void Start () {
        ta = GetComponent<TweenColor>();
        lbTxt = GetComponentInChildren<UILabel>();
	}
	
    public void ShowHitPoint(string damage)
    {
        lbTxt.text = damage;
        ta.ResetToBeginning();
        ta.PlayForward();
    }

}
